package me.tojatta.milk.module;

public enum HackType {
	
	PLAYER("Player"),
	WORLD("World"),
	COMBAT("Combat"),
	MOVEMENT("Movement"),
	RENDER("Render"),
	MISC("Misc"),
	GUI("Ui"),
	OTHER("Other");
	
	private String name;
	private HackType(String name){
		this.name = name;
	}
	public String getName(){
		return this.name;
	}
	
	

}
