package me.tojatta.milk.module.modules.other;

import org.lwjgl.input.Keyboard;

import me.tojatta.milk.Milk;
import me.tojatta.milk.module.HackType;
import me.tojatta.milk.module.Module;

public class Gui extends Module{

	public Gui() {
		super("Gui", Keyboard.KEY_RSHIFT, -1, HackType.OTHER, false);
	}
	@Override
	public void onEnable(){
		getMC().displayGuiScreen(Milk.getInstance().getClickGui());
	}

}
