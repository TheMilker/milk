package me.tojatta.milk.module.modules.movement;


import me.tojatta.milk.Milk;
import me.tojatta.milk.events.EventPostUpdate;
import me.tojatta.milk.events.EventTick;
import me.tojatta.milk.module.HackType;
import me.tojatta.milk.module.Module;
import net.minecraft.block.Block;
import net.minecraft.block.BlockAir;
import net.minecraft.block.BlockAnvil;
import net.minecraft.block.BlockChest;
import net.minecraft.block.material.Material;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;

import org.lwjgl.input.Keyboard;

import com.darkmagician6.eventapi.EventTarget;

public class Step extends Module{

	public Step() {
		super("Step", Keyboard.KEY_B, 0xff33ff33, HackType.MOVEMENT, true);
	}
	private float height = 2F;
	@Override
	public void onDisable(){
		getPlayer().stepHeight = 0.5F;
	}
	@EventTarget
	public void onTick(EventTick event){
		if(Milk.getInstance().getModMan().getModule("NoCheat").isEnabled()){
			Block block = getWorld().getBlock((int)getPlayer().posX, (int)getPlayer().posY, (int)getPlayer().posZ);
	        if ((getPlayer().onGround) && (getPlayer().isCollidedHorizontally) && ((block instanceof BlockAir))){
	        	getPlayer().isCollidedHorizontally = false;
	        	sendPacket(new C03PacketPlayer.C04PacketPlayerPosition(getPlayer().posX, getPlayer().boundingBox.minY + 0.5, getPlayer().posY + 0.5, getPlayer().posZ, getPlayer().onGround));
	        	getPlayer().stepHeight = 1;
	        }
	        /**
	         * Creds to Capsar.
	         */
	        if(getPlayer().isCollidedHorizontally && getWorld().getBlock(MathHelper.floor_double(getPlayer().posX),MathHelper.floor_double(getPlayer().posY + 2), MathHelper.floor_double(getPlayer().posZ)).getMaterial() == Material.air) {
	    		for(Object e : getWorld().getCollidingBoundingBoxes(getPlayer(), getPlayer().boundingBox.expand(0.01, 0, 0.01))) {
	    			AxisAlignedBB d = (AxisAlignedBB) e;
	    			Block b = getWorld().getBlock(MathHelper.floor_double(d.minX), MathHelper.floor_double(d.minY), MathHelper.floor_double(d.minZ));
	    			Block b2 = getWorld().getBlock(MathHelper.floor_double(d.minX), MathHelper.floor_double(d.minY - 1), MathHelper.floor_double(d.minZ));
	    			if(b instanceof BlockAnvil || b instanceof BlockChest || b2 instanceof BlockAnvil || b2 instanceof BlockChest) {
	    				getPlayer().setPosition(getPlayer().posX, getPlayer().posY + 0.2, getPlayer().posZ);
	    				getPlayer().motionY =+ 0.026;
	    				getPlayer().setSprinting(true);
	    			}
	    		}
	        }
		}else{
			getPlayer().stepHeight = height;
		}
	}
	@EventTarget
	public void postUpdate(EventPostUpdate event){
		if(Milk.getInstance().getModMan().getModule("NoCheat").isEnabled()){
			getPlayer().stepHeight = 0.5F;
		}
	}
}
