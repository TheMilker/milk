package me.tojatta.milk.module.modules.movement;

import me.tojatta.milk.events.EventTick;
import me.tojatta.milk.module.HackType;
import me.tojatta.milk.module.Module;

import org.lwjgl.input.Keyboard;

import com.darkmagician6.eventapi.EventTarget;

public class Speed extends Module{

	public Speed() {
		super("Speed", Keyboard.KEY_G, 0xff33ff33, HackType.MOVEMENT, true);
	}
	@EventTarget
	public void onTick(EventTick event){
		this.setName("Speed(Sprint)");
		/**
		 * Checks if the player is moving and hunger.
		 */
		if(getPlayer().moveForward > 0F && 
				getPlayer().getFoodStats().getFoodLevel() >= 6 && 
				!getPlayer().isCollidedHorizontally){
			/**
			 * Makes the player sprint.
			 */
			getPlayer().setSprinting(true);
		}
	}

}
