package me.tojatta.milk.module.modules.combat;

import java.util.List;

import me.tojatta.milk.events.EventPostUpdate;
import me.tojatta.milk.events.EventPreUpdate;
import me.tojatta.milk.events.EventRotation;
import me.tojatta.milk.events.EventTick;
import me.tojatta.milk.module.HackType;
import me.tojatta.milk.module.Module;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.play.client.C0BPacketEntityAction;

import org.lwjgl.input.Keyboard;

import com.darkmagician6.eventapi.EventTarget;

public class Killaura extends Module{

	public Killaura() {
		super("Killaura", Keyboard.KEY_F, 0xffff3333, HackType.COMBAT, true);
	}
	private Entity selected;
	private boolean attacking;
	private float range = 4.0F;
	private float speed = 10F;
	private long lastMS = -1L;
	private float yaw, pitch;
	@EventTarget
	public void onLook(EventRotation event){
		event.setRotation(Float.valueOf(yaw), Float.valueOf(pitch));
	}
	@EventTarget
	public void preUpdate(EventPreUpdate event){
		this.silentAim(this.selected);
	}
	@EventTarget
	public void onTick(EventTick event){
		this.selected = this.getClosest();
		if(this.canTarget(this.selected)){
			this.attackEntity(this.selected);
			this.attacking = true;
		}else{
			this.attacking = false;
		}
	}
	
	public void silentAim(Entity target){
		double x = (target.posX + target.motionX) - getPlayer().posX;
		double z = (target.posZ + target.motionY) - getPlayer().posZ;
		double h = (getPlayer().posY + (double)getPlayer().getEyeHeight()) - (target.posY + (double)target.getEyeHeight());
		double h1 = Math.sqrt(x * x + z * z);
		float f = (float)((Math.atan2(z, x) * 180D) / Math.PI) - 90F;
		float f1 = (float)((Math.atan2(h, h1) * 180D) / Math.PI);
		this.pitch = f1;
		this.yaw = f;
	}
	
	private void attackEntity(Entity entity){
		long currentMS = System.nanoTime() / 1000000L;
		if ((currentMS - this.lastMS >= 1000L / this.speed) || (this.lastMS == -1L)){
			boolean sprint = getPlayer().isSprinting();
			getPlayer().swingItem();
			if(sprint){
				getPlayer().setSprinting(false);
                sendPacket(new C0BPacketEntityAction(getPlayer(), 5));
			}
			getMC().playerController.attackEntity(getPlayer(), entity);
			if (sprint){
                getPlayer().setSprinting(true);
                sendPacket(new C0BPacketEntityAction(getPlayer(), 4));
            }
			this.lastMS = currentMS;
		}
	}
	
	public boolean canTarget(Entity entity) {
		if (!entity.isEntityAlive()) {
			return false;
		}if (!getPlayer().canEntityBeSeen(entity)) {
			return false;
		}if (entity instanceof EntityPlayer) {
			if (entity.equals(this.getPlayer())
					|| entity instanceof EntityClientPlayerMP) {
				return false;
			}//TODO: Add friend system.
			return true;
		}return true;
	}
	
	private Entity getClosest() {
		float range = this.range;
		Entity closest = null;
		for (Entity entity : (List<Entity>) getWorld().loadedEntityList) {
			if (entity instanceof EntityLivingBase) {
				EntityLivingBase e = (EntityLivingBase) entity;
			if (!this.canTarget(entity)) {
				continue;
			}
			float distance = this.getPlayer().getDistanceToEntity(entity);
			if (distance < range) {
				closest = entity;
				range = distance;
			}
		}
		}
		return closest;
	}

}
