package me.tojatta.milk.module.modules.ui;

import net.minecraft.client.gui.ScaledResolution;
import me.tojatta.milk.Milk;
import me.tojatta.milk.Wrapper;
import me.tojatta.milk.events.EventGui;
import me.tojatta.milk.module.HackType;
import me.tojatta.milk.module.Module;

import com.darkmagician6.eventapi.EventTarget;

public class ArrayListUi extends Module{

	public ArrayListUi() {
		super("Arraylist", 0, -1, HackType.GUI, false);
		this.setEnabled(true);
	}
	@EventTarget
	public void onScreenRender(EventGui event){
		/**
		 * Gets screen res/size.
		 */
		ScaledResolution sr = new ScaledResolution(getMC(), getMC().displayWidth, getMC().displayHeight);
	    int width = sr.getScaledWidth();
	    int height = sr.getScaledHeight();
	    
	    int y = 2;
	    /**
	     * Loops through enabled mods.
	     */
	    for(Module mod : Milk.getInstance().getModMan().hacksOn){
	    	/**
	    	 * Ignores curtain types of hacks.
	    	 */
	    	if(mod.getType() == HackType.GUI || mod.getType() == HackType.OTHER || !mod.isArray()){
	    		continue;
	    	}
	    	/**
	    	 * Writes enabled mods on the right side of screen.
	    	 */
	    	Wrapper.getFR().drawStringWithShadow("�c+�f" + mod.getName() + "�c+�f", 
	    			width - Wrapper.getFR().getStringWidth("+" + mod.getName() + "+") - 2, 
	    			y, mod.getColor());
	    	/**
	    	 * Adds 10 to the y for every hack enabled.
	    	 */
	    	y += 12;
	    }
	}

}
