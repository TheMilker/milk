package me.tojatta.milk.module;


import com.darkmagician6.eventapi.EventManager;

import me.tojatta.milk.Milk;
import me.tojatta.milk.Wrapper;

public class Module extends Wrapper{
	
	private String name;
	private int key;
	private int color;
	private HackType type;
	private boolean array;
	private boolean enabled;
	
	public Module(String name, int key, int color, HackType type, boolean array){
		this.name = name;
		this.key = key;
		this.color = color;
		this.type = type;
		this.array = array;
		System.out.print("[Milk]: Module Loaded: " + name);
	}
	/**
	 * Gets the module name.
	 * @return
	 */
	public String getName() {
		return name;
	}
	/**
	 * Sets the module name.
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * Gets the module keybind.
	 * @return
	 */
	public int getKey() {
		return key;
	}
	/**
	 * Sets the module keybind.
	 * @param key
	 */
	public void setKey(int key) {
		this.key = key;
	}
	/**
	 * Checks if its the key.
	 * @param key
	 * @return
	 */
	public boolean isKey(int key){
		if(key == (this.key)){
			return true;
		}else{
			return false;
		}
	}
	/**
	 * Gets the module color
	 * @return
	 */
	public int getColor() {
		return color;
	}
	/**
	 * Sets module color.
	 * @param color
	 */
	public void setColor(int color) {
		this.color = color;
	}
	/**
	 * Gets if module is on arraylist.
	 * @return
	 */
	public boolean isArray() {
		return array;
	}
	/**
	 * Sets module on arraylist.
	 * @param array
	 */
	public void setArray(boolean array) {
		this.array = array;
	}
	/**
	 * Gets the type of module.
	 * @return
	 */
	public HackType getType() {
		return type;
	}
	/**
	 * Sets the type of module.
	 * @param type
	 */
	public void setType(HackType type) {
		this.type = type;
	}
	/**
	 * Gets the module state.
	 * @return
	 */
	public boolean isEnabled() {
		return enabled;
	}
	/**
	 * Sets the module state.
	 * @param enabled
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
		if(isEnabled()){
			onEnable();
			EventManager.register(this);
			Milk.getInstance().getModMan().hacksOn.add(this);
			if(Milk.getInstance().debug() && (getWorld() != null)){
				addChat("�e" + name + "�f" + " has been �aenabled�f.");
			}
		}else{
			onDisable();
			EventManager.unregister(this);
			Milk.getInstance().getModMan().hacksOn.remove(this);
			if(Milk.getInstance().debug() && (getWorld() != null)){
				addChat("�e" + name + "�f" + " has been �cdisabled�f.");
			}
		}
	}
	/**
	 * Toggles the module.
	 */
	public void toggleMod(){
		this.setEnabled(!enabled);
	}
	
	public void onEnable(){}
	public void onDisable(){}
}
