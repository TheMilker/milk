package me.tojatta.milk.module;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import me.tojatta.milk.Milk;
import me.tojatta.milk.commands.CommandManager;
import me.tojatta.milk.commands.commands.Help;
import me.tojatta.milk.commands.commands.Say;
import me.tojatta.milk.module.modules.combat.Killaura;
import me.tojatta.milk.module.modules.misc.Nocheat;
import me.tojatta.milk.module.modules.movement.Speed;
import me.tojatta.milk.module.modules.movement.Step;
import me.tojatta.milk.module.modules.other.Gui;
import me.tojatta.milk.module.modules.ui.ArrayListUi;


public class ModuleManager {
	
	private final List<Module> hacks = new CopyOnWriteArrayList();
	public ArrayList<Module> hacksOn = new ArrayList();
	public final List<Module> getHacks(){
		return hacks;
	}
	
	public void loadModules(){
		this.hacks.clear();
		this.hacks.add(new Gui());
		this.hacks.add(new Nocheat());
		this.hacks.add(new Step());
		this.hacks.add(new ArrayListUi());
		this.hacks.add(new Speed());
		this.hacks.add(new Killaura());
	}
	
	public void loadCommands(){
		CommandManager.getInstance().getCommands().clear();
		CommandManager.getInstance().getCommands().add(new Help());
		CommandManager.getInstance().getCommands().add(new Say());
	}
	
	public void onKeypress(int k){
		for(Module m : Milk.getInstance().getModMan().getHacks()){
			if(m.isKey(k)){
				m.toggleMod();
				break;
			}
		}
	}
	
	public Module getModule(String name) {
		for (Module module : getHacks()) {
			if (module.getName().equalsIgnoreCase(name)) {
				return module;
			}
		}
		return null;
	}

}
