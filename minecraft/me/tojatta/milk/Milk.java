package me.tojatta.milk;


import me.tojatta.milk.module.ModuleManager;
import me.tojatta.milk.ui.click.GuiClick;
import me.tojatta.milk.ui.click.theme.manager.ThemeManager;
import me.tojatta.milk.ui.click.theme.themes.milk.ThemeMilk;
import me.tojatta.milk.ui.ttf.TTFRenderer;

public class Milk {
	/**
	 * Client name and version
	 */
	private String CLIENTNAME = "Milk";
	private float VERSION = 0.3F;
	
	private static Milk instance = new Milk();
	private static ModuleManager modMan = new ModuleManager();
	private GuiClick clickGui;
	private static TTFRenderer guiFont = null;
	
	/**
	 * Loads these things on startup.
	 */
	public void startUp(){
		if (Wrapper.getMC().session.getUsername().startsWith("Player")) {
		      Wrapper.loginToMinecraft("geniequintero@hotmail.com", "scout2");
		}
		
		modMan.loadModules();
		modMan.loadCommands();
		ThemeManager.getInstance().addTheme(new ThemeMilk());
		this.getClickGui();
	}
	/**
	 * Getters and setters.
	 */
	public String getCLIENTNAME() {
		return CLIENTNAME;
	}

	public void setCLIENTNAME(String cLIENTNAME) {
		CLIENTNAME = cLIENTNAME;
	}

	public float getVERSION() {
		return VERSION;
	}

	public void setVERSION(float vERSION) {
		VERSION = vERSION;
	}

	public static Milk getInstance() {
		return instance;
	}
	
	public ModuleManager getModMan() {
		return modMan;
	}
	
	public GuiClick getClickGui() {
		if(clickGui == null) {
		   clickGui = new GuiClick();
		   clickGui.loadPanels();
		}return clickGui;
	}
		 
	public void setClickGui(GuiClick click) {
		this.clickGui = click;
	}
	
	public final TTFRenderer getGuiFont(){
		if (guiFont == null){
			guiFont = new TTFRenderer("Arial Bold", 18);
	    }return guiFont;
	}
	
	public boolean debug(){
		return true;
	}
	
}
