package me.tojatta.milk;


import java.net.Proxy;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.network.Packet;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.Session;

import com.mojang.authlib.Agent;
import com.mojang.authlib.exceptions.AuthenticationException;
import com.mojang.authlib.yggdrasil.YggdrasilAuthenticationService;
import com.mojang.authlib.yggdrasil.YggdrasilUserAuthentication;

public class Wrapper {
	
	private static Minecraft mc = Minecraft.getMinecraft();
	
	public static Minecraft getMC(){
		return mc;
	}
	
	public static EntityClientPlayerMP getPlayer(){
		return mc.thePlayer;
	}
	
	public static WorldClient getWorld(){
		return mc.theWorld;
	}
	
	public static FontRenderer getFR(){
		return mc.fontRenderer;
	}
	
	public static GameSettings getGS(){
		return mc.gameSettings;
	}
	
	public static void addChat(String s){
		getPlayer().addChatMessage(new ChatComponentText(("�c[�fMilk�c]�f: " + s)));
	}
	
	public static void sendPacket(Packet packet) {
		mc.getNetHandler().addToSendQueue(packet);
	}
	
	public static void loginToMinecraft(String username, String password)
	  {
	    if ((username == null) || (username.length() <= 0) || (password == null) || (password.length() <= 0)) {
	      return;
	    }
	    YggdrasilAuthenticationService a = new YggdrasilAuthenticationService(Proxy.NO_PROXY, "");
	    YggdrasilUserAuthentication b = (YggdrasilUserAuthentication)a.createUserAuthentication(Agent.MINECRAFT);
	    b.setUsername(username);
	    b.setPassword(password);
	    try
	    {
	      b.logIn();
	      String response = "swag:yolo:" + b.getSelectedProfile().getName() + ":" + b.getAuthenticatedToken() + ":" + b.getSelectedProfile().getId();
	      String[] values = response.split(":");
	      getMC().session = new Session(values[2].trim(), values[4].trim(), values[3].trim(), "MOJANG");
	    }
	    catch (AuthenticationException e)
	    {
	      e.printStackTrace();
	    }
	  }

}
