package me.tojatta.milk.ui.click.elements;

import me.tojatta.milk.Wrapper;


public class ElementBase {

	protected ElementBase(int HEIGHT, int WIDTH) {
		this.ELEMENT_HEIGHT = HEIGHT;
		this.ELEMENT_WIDTH = WIDTH;
	}
	
	protected ElementBase(int HEIGHT) {
		this.ELEMENT_HEIGHT = HEIGHT;
		this.ELEMENT_WIDTH = 104;
	}
	
	public void drawElement(int POSX, int POSY, int MOUSEX, int MOUSEY) { }
	
	public void mouseClicked(int POSX, int POSY, int BUTTON) { }

	public Wrapper getWrapper() {
		return new Wrapper();
	}
	
	private int ELEMENT_POSX;
	
	public int getPosX() {
		return ELEMENT_POSX;
	}
	
	public void setPosX(int POSX) {
		this.ELEMENT_POSX = POSX;
	}
	
	private int ELEMENT_POSY;
	
	public int getPosY() {
		return ELEMENT_POSY;
	}
	
	public void setPosY(int POSY) {
		this.ELEMENT_POSY = POSY;
	}
	
	private int ELEMENT_HEIGHT;
	
	public int getHeight() {
		return ELEMENT_HEIGHT;
	}
	
	public void setHeight(int HEIGHT) {
		this.ELEMENT_HEIGHT = HEIGHT;
	}
	
	private int ELEMENT_WIDTH;
	
	public int getWidth() {
		return ELEMENT_WIDTH;
	}
	
	public void setWidth(int WIDTH) {
		this.ELEMENT_WIDTH = WIDTH;
	}
}
