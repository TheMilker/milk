package me.tojatta.milk.ui.click;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import me.tojatta.milk.Milk;
import me.tojatta.milk.module.HackType;
import me.tojatta.milk.ui.click.panel.PanelBase;
import me.tojatta.milk.ui.click.theme.manager.ThemeManager;
import me.tojatta.milk.ui.click.theme.themes.milk.gui.GuiClickMilk;
import me.tojatta.milk.ui.click.theme.themes.milk.window.WindowThemeMilk;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiScreen;

public class GuiClick extends GuiScreen {

	private static final List<PanelBase> GUI_PANELS = new ArrayList();
	
	public List<PanelBase> getPanels() {
		return GUI_PANELS;
	}
	
	public GuiClick() {
		loadPanels();
	}
	
	private static final File milkDir = new File(Minecraft.getMinecraft().mcDataDir + File.separator +"Milk");
	private static final File guiFile = new File(milkDir + File.separator + "gui.txt");
	
	public void loadPanels() {
		if (!milkDir.exists()) {
			milkDir.mkdir();
		}
		if(!getAlreadyLoaded()) {
			loadGuiSettings();
			setAlreadyLoaded(true);
		}
		
		GUI_PANELS.clear();
		int i = 2;
		HackType[] e = HackType.values();
		int len$ = e.length;

		for (int i$ = 0; i$ < len$; ++i$) {
			HackType type = e[i$];
			if(!type.getName().equalsIgnoreCase("other")){
				if(ThemeManager.getInstance().getCurrentTheme().getName().equalsIgnoreCase("Milk")) {
					GUI_PANELS.add(new GuiClickMilk(this, type.getName(), 2, i, type));
				}
				i += 17;
			}
		}
		
		loadGuiSettings();
	}
	
	@Override
	public void drawScreen(int par1, int par2, float par3) {
		 drawDefaultBackground();
		    try
		    {
		      for (PanelBase panel : getPanels()) {
		        panel.drawScreen(par1, par2);
		      }
		    }
		    catch (Exception e)
		    {
		      e.printStackTrace();
		    }
		    super.drawScreen(par1, par2, par3);
	}

	@Override
	public void mouseMovedOrUp(int par1, int par2, int par3) {
		Iterator i$ = getPanels().iterator();
		
		while(i$.hasNext()){
			PanelBase panel = (PanelBase) i$.next();
			panel.mouseMovedOrUp(par1, par2, par3);
		}

		super.mouseMovedOrUp(par1, par2, par3);
	}

	@Override
	public void mouseClicked(int par1, int par2, int par3) {
		Iterator i$ = getPanels().iterator();
		
		while(i$.hasNext()) {
			PanelBase panel = (PanelBase) i$.next();
			panel.mouseClicked(par1, par2, par3);
		}

		saveGuiSettings();
		
		super.mouseClicked(par1, par2, par3);
	}

	@Override
	public void onGuiClosed() {
		Iterator i$ = getPanels().iterator();
		
		while(i$.hasNext()) {
			PanelBase panel = (PanelBase) i$.next();
			panel.onGuiClosed();
		}

		saveGuiSettings();
		
		if(Milk.getInstance().getModMan().getModule("Gui").isEnabled()) {
			Milk.getInstance().getModMan().getModule("Gui").setEnabled(false);
		}

		super.onGuiClosed();
	}
	
	private static boolean THEME_FIRSTLOADED;
	
	public boolean getAlreadyLoaded() {
		return THEME_FIRSTLOADED;
	}
	
	public void setAlreadyLoaded(boolean LOADED) {
		this.THEME_FIRSTLOADED = LOADED;
	}
	
	public boolean doesGuiPauseGame() {
		return false;
	}
	
	public void saveGuiSettings(){
	    try{
	      File file = new File(Minecraft.getMinecraft().mcDataDir + File.separator +"Milk", "gui.txt");
	      BufferedWriter out = new BufferedWriter(new FileWriter(file));
	      for (Iterator iterator = getPanels().iterator(); iterator.hasNext(); out.write("\r\n")){
	        PanelBase window = (PanelBase)iterator.next();
	        out.write(window.getName().replace(" ", "") + ":" + window.PANEL_POSX + ":" + window.PANEL_POSY);
	      }
	      out.close();
	    }
	    catch (Exception exception) {}
	  }
	  
	  public void loadGuiSettings()
	  {
	    try
	    {
	      File file = new File(Minecraft.getMinecraft().mcDataDir + File.separator +"Milk", "gui.txt");
	      FileInputStream fstream = new FileInputStream(file.getAbsolutePath());
	      DataInputStream in = new DataInputStream(fstream);
	      BufferedReader br = new BufferedReader(new InputStreamReader(in));
	      String line;
	      String title;
	      int dragX;
	      int dragY;
	      
	      Iterator iterator;
	      while ((line = br.readLine()) != null)
	      {
	        String curLine = line.toLowerCase().trim();
	        String[] args = curLine.split(":");
	        title = args[0];
	        dragX = Integer.parseInt(args[1]);
	        dragY = Integer.parseInt(args[2]);
	        for (iterator = getPanels().iterator(); iterator.hasNext();)
	        {
	          PanelBase window = (PanelBase)iterator.next();
	          if (window.getName().replace(" ", "").equalsIgnoreCase(title))
	          {
	            window.PANEL_POSX = dragX;
	            window.PANEL_POSY = dragY;
	            window.setOpen(true);	
	          }
	        }
	      }
	    }
	    catch (Exception e)
	    {
	      e.printStackTrace();
	      saveGuiSettings();
	    }
	  }
	
}
