package me.tojatta.milk.ui.click.panel;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import me.tojatta.milk.Wrapper;
import me.tojatta.milk.ui.click.elements.ElementBase;
import me.tojatta.milk.ui.click.theme.manager.ThemeManager;

import org.lwjgl.input.Mouse;

public abstract class PanelBase {
	
	protected PanelBase(String NAME, int POSX, int POSY, int WIDTH) {
		this.PANEL_NAME = NAME;
		this.PANEL_POSX = POSX;
		this.PANEL_POSY = POSY;
		this.PANEL_HEIGHT = 15;
		this.PANEL_WIDTH = WIDTH;
	}
	
	protected PanelBase(String NAME, int POSX, int POSY) {
		this.PANEL_NAME = NAME;
		this.PANEL_POSX = POSX;
		this.PANEL_POSY = POSY;
		this.PANEL_HEIGHT = 15;
		this.PANEL_WIDTH = 110;
	}
	
	public void drawScreen(int POSX, int POSY) { 
		
	}
	
	public Wrapper getWrapper() {
		return new Wrapper();
	}
	
	public void onGuiClosed() {
		setDrag(false);
	}
	
	public void mouseMovedOrUp(int POSX, int POSY, int BUTTON) { }
	
	protected void updatePanel(int POSX, int POSY) {
		if(getDrag()) {
           setPosX(POSX + getDragX());
           setPosY(POSY + getDragY());
           
           if(!Mouse.isButtonDown(0)) {
        	   setDrag(false);
           }
        }
		
		if(getOpen()) {
			setHeight(15 + getOpenHeight());
		}else{
			setHeight(15);
		}
	}
	
	public abstract void drawElements(int POSX, int POSY);
	
	public abstract void addElements();
	
	public void mouseClicked(int POSX, int POSY, int BUTTON) { 
		if(POSX >= getPosX() && POSY >= getPosY() && POSX <= getPosX() + getWidth() && POSY <= getPosY() + 15 && BUTTON == 0) {
			if(!(POSX >= getPosX() + getWidth() - 13.0F && POSY >= getPosY() + 3.0F && POSX <= getPosX() + getWidth() - 3.0F && POSY <= getPosY() + 15 - 3.0F && BUTTON == 0) && !(POSX >= getPosX() + getWidth() - 25.0F && POSY >= getPosY() + 3.0F && POSX <= getPosX() + getWidth() - 15.0F && POSY <= getPosY() + 15 - 3.0F && BUTTON == 0)) {
				setDrag(true);
				setDragX(getPosX() - POSX);
				setDragY(getPosY() - POSY);
			}else{
				if(ThemeManager.getInstance().getCurrentTheme().getName().equalsIgnoreCase("Milk")) {
					if(POSX >= getPosX() + getWidth() - 14.0F && POSY >= getPosY() + 3.0F && POSX <= getPosX() + getWidth() - 3.0F && POSY <= getPosY() + 15 - 3.0F && BUTTON == 0) {
						setPin(!getPin());
					}
				
					if(POSX >= getPosX() + getWidth() - 27.0F && POSY >= getPosY() && POSX <= getPosX() + getWidth() - 16.0F && POSY <= getPosY() + 15 - 3.0F && BUTTON == 0) {
						setOpen(!getOpen());
					}
				}
				getWrapper().getPlayer().playSound("random.click", 1.0F, 1.0F);
			}
		}
		
		Iterator i$ = getElements().iterator();
		
		while(i$.hasNext()) {
			ElementBase i$1 = (ElementBase) i$.next();
			
			if(getOpen() && (POSX > 110 || POSY > 17)) {
				i$1.mouseClicked(POSX, POSY, BUTTON);
			}
		}
	}

	private String PANEL_NAME;
	
	public String getName() {
		return PANEL_NAME;
	}
	
	public void setName(String NAME) {
		this.PANEL_NAME = NAME;
	}
	
	private List<ElementBase> PANEL_ELEMENTS = new ArrayList();
	
	public List<ElementBase> getElements() {
		return PANEL_ELEMENTS;
	}
	
	public void addElement(ElementBase ELEMENT) {
		this.PANEL_ELEMENTS.add(ELEMENT);
	}
	
	public void removeElement(ElementBase ELEMENT) {
		this.PANEL_ELEMENTS.remove(ELEMENT);
	}
	
	public int PANEL_POSX;
	
	public int getPosX() {
		return PANEL_POSX;
	}
	
	public void setPosX(int POSX) {
		this.PANEL_POSX = POSX;
	}
	
	public int PANEL_POSY;
	
	public int getPosY() {
		return PANEL_POSY;
	}
	
	public void setPosY(int POSY) {
		this.PANEL_POSY = POSY;
	}
	
	private int PANEL_HEIGHT;
	
	public int getHeight() {
		return PANEL_HEIGHT;
	}
	
	public void setHeight(int HEIGHT) {
		this.PANEL_HEIGHT = HEIGHT;
	}
	
	private int PANEL_OPENHEIGHT;
	
	public int getOpenHeight() {
		return PANEL_OPENHEIGHT;
	}
	
	public void setOpenHeight(int OPENHEIGHT) {
		this.PANEL_OPENHEIGHT = OPENHEIGHT;
	}
	
	private int PANEL_WIDTH;
	
	public int getWidth() {
		return PANEL_WIDTH;
	}
	
	public void setWidth(int WIDTH) {
		this.PANEL_WIDTH = WIDTH;
	}
	
	private boolean PANEL_DRAG;
	
	public boolean getDrag() {
		return PANEL_DRAG;
	}
	
	public void setDrag(boolean DRAG) {
		this.PANEL_DRAG = DRAG;
	}
	
	private int PANEL_DRAGX;
	
	public int getDragX() {
		return PANEL_DRAGX;
	}
	
	public void setDragX(int DRAGX) {
		this.PANEL_DRAGX = DRAGX;
	}
	
	private int PANEL_DRAGY;
	
	public int getDragY() {
		return PANEL_DRAGY;
	}
	
	public void setDragY(int DRAGY) {
		this.PANEL_DRAGY = DRAGY;
	}
	
	private boolean PANEL_OPEN;
	
	public boolean getOpen() {
		return PANEL_OPEN;
	}
	
	public void setOpen(boolean OPEN) {
		this.PANEL_OPEN = OPEN;
	}
	
	private boolean PANEL_PIN;
	
	public boolean getPin() {
		return PANEL_PIN;
	}
	
	public void setPin(boolean PIN) {
		this.PANEL_PIN = PIN;
	}
	
	private boolean PANEL_VISIBLE;
	
	public boolean getVisible() {
		return PANEL_VISIBLE;
	}
	
	public void setVisible(boolean VISIBLE) {
		this.PANEL_VISIBLE = VISIBLE;
	}
}
