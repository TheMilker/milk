package me.tojatta.milk.ui.click.theme.manager;

import java.util.ArrayList;
import java.util.List;

import me.tojatta.milk.Wrapper;
import me.tojatta.milk.ui.click.theme.Theme;


public class ThemeManager extends Wrapper {
	
	private static List<Theme> THEME_LIST = new ArrayList<Theme>();
	private static List<String> THEME_NAMELIST = new ArrayList<String>();
	
	public List<Theme> getList() {
		return THEME_LIST;
	}
	
	public List<String> getNameList() {
		return THEME_NAMELIST;
	}
	
	public void addTheme(Theme THEME) {
		this.THEME_LIST.add(THEME);
		this.THEME_NAMELIST.add(THEME.getName());
	}
	
	public void removeTheme(Theme THEME) {
		this.THEME_LIST.remove(THEME);
		this.THEME_NAMELIST.remove(THEME.getName());
	}
	
	public Theme getCurrentTheme() {
		for(Theme THEME: getList()) {
			if(THEME.getState()) {
				return THEME;
			}
		}
		return null;
	}
	
	public Theme findTheme(String NAME) {
		for(Theme THEME: getList()) {
			if(THEME.getName().equalsIgnoreCase(NAME) || THEME.getName().trim().equalsIgnoreCase(NAME)) {
				if(THEME != null) {
					return THEME;
				}
			}
		}
		return null;
	}
	public static ThemeManager getInstance() {
		return new ThemeManager();
	}
}
