package me.tojatta.milk.ui.click.theme.themes.milk.elements;

import me.tojatta.milk.Milk;
import me.tojatta.milk.module.Module;
import me.tojatta.milk.ui.GuiUtils;
import me.tojatta.milk.ui.click.elements.ElementBase;


public class ElementButton extends ElementBase {
	
    public ElementButton(Module module) {
        super(14);
        setModule(module);
    }

    @Override
    public void drawElement(int par1, int par2, int par3, int par4) {
    	setPosX(par1);
    	setPosY(par2);
    	
    	GuiUtils.drawBorderedRect(getPosX(), getPosY(), getPosX() + getWidth(), getPosY() + getHeight(), 0x80000000, getModule().isEnabled() ? 0xff19A3B5 : 0xff165566);
    
    	Milk.getInstance().getGuiFont().drawCenteredString(getModule().getName(), (getWidth()) / 2 + getPosX(), getPosY() + 3, getModule().isEnabled() ? 0xFFFFFFFF : 0xFFFFFFFF);
    }

    @Override
    public void mouseClicked(int par1, int par2, int par3) {
    	if(par1 >= getPosX() && par2 >= getPosY() && par1 <= getPosX() + getWidth() && par2 <= getPosY() + getHeight() && par3 == 0) {
    		getModule().toggleMod();
			getWrapper().getPlayer().playSound("random.click", 1.0F, 1.0F);
    	}
    }
    
    private Module BUTTON_MODULE;
    
    public Module getModule() {
    	return BUTTON_MODULE;
    }
    
    public void setModule(Module MODULE) {
    	this.BUTTON_MODULE = MODULE;
    }
}
