package me.tojatta.milk.ui.click.theme.themes.milk.window;

import java.util.Iterator;

import me.tojatta.milk.ui.click.elements.ElementBase;
import me.tojatta.milk.ui.click.theme.manager.ThemeManager;
import me.tojatta.milk.ui.click.theme.themes.milk.elements.ElementComboBox;
import me.tojatta.milk.ui.click.theme.themes.milk.panel.PanelBaseMilk;

import org.apache.commons.lang3.StringUtils;

public class WindowThemeMilk extends PanelBaseMilk {

	public WindowThemeMilk(String NAME, int POSX, int POSY) {
		super(NAME, POSX, POSY);
	}

	@Override
	public void addElements() {
		
		if(getElements().size() < 1) {
			//this.getElements().add(new ElementColorPicker());
			String[] themeList = StringUtils.join(ThemeManager.getInstance().getNameList(), ", ").split(", ");
			this.getElements().add(new ElementComboBox(themeList));
		}
	}

	@Override
	public void drawElements(int POSX, int POSY) {
		int currentPosition = 0;
		Iterator i$ = getElements().iterator();
		
		while(i$.hasNext()) {
			ElementBase i$1 = (ElementBase) i$.next();
			
			if((i$1 instanceof ElementComboBox)) {
				if(getOpen()) {
					i$1.drawElement(getPosX() + 3, getPosY() + currentPosition + 16, POSX, POSY);
					currentPosition += i$1.getHeight() + 1;
					setOpenHeight(currentPosition + 3);
				}
			}/*else if((i$1 instanceof ElementColorPicker)) {
				if(getOpen()) {
					i$1.drawElement(getPosX() + 3, getPosY() + currentPosition + 16, POSX, POSY);
					currentPosition += i$1.getHeight() + 1;
					setOpenHeight(currentPosition + 3);
				}
			}*/
		}
	}
}

