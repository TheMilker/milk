package me.tojatta.milk.ui.click.theme.themes.milk.gui;

import java.util.Iterator;

import me.tojatta.milk.Milk;
import me.tojatta.milk.module.HackType;
import me.tojatta.milk.module.Module;
import me.tojatta.milk.ui.click.GuiClick;
import me.tojatta.milk.ui.click.elements.ElementBase;
import me.tojatta.milk.ui.click.theme.themes.milk.elements.ElementButton;
import me.tojatta.milk.ui.click.theme.themes.milk.panel.PanelBaseMilk;


public class GuiClickMilk extends PanelBaseMilk {
	
	public GuiClickMilk(GuiClick SCREEN, String NAME, int POSX, int POSY, HackType TYPE) {
		super(NAME, POSX, POSY);
		this.GUI_SCREEN = SCREEN;
		this.GUI_TYPE = TYPE;
	}
	
	@Override
	public void addElements() {
		loadElements();
	}

	public void loadElements() {
		this.getElements().clear();
		Iterator i$ = Milk.getInstance().getModMan().getHacks().iterator();
		
		while(i$.hasNext()) {
			Module MOD = (Module) i$.next();
			
			if(MOD.getType() == getType()) {
				this.addElement(new ElementButton(MOD));
			}
		}
	}
	
	private GuiClick GUI_SCREEN;
	
	public GuiClick getScreen() {
		return GUI_SCREEN;
	}
	
	public void setScreen(GuiClick SCREEN) {
		this.GUI_SCREEN = SCREEN;
	}
	
	private HackType GUI_TYPE;
	
	public HackType getType() {
		return GUI_TYPE;
	}
	
	public void setType(HackType TYPE) {
		this.GUI_TYPE = TYPE;
	}

	@Override
	public void drawElements(int POSX, int POSY) {
		int currentPosition = 0;
		Iterator i$ = getElements().iterator();
		
		while(i$.hasNext()) {
			ElementBase i$1 = (ElementBase) i$.next();
			
			if((i$1 instanceof ElementButton)) {
				if(getOpen()) {
					i$1.drawElement(getPosX() + 3, getPosY() + currentPosition + 15, POSX, POSY);
					currentPosition += i$1.getHeight() + 1;
					setOpenHeight(currentPosition + 3);
				}
			}
		}
	}
}
