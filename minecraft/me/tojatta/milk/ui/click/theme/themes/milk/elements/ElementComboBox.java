package me.tojatta.milk.ui.click.theme.themes.milk.elements;

import me.tojatta.milk.Milk;
import me.tojatta.milk.ui.GuiUtils;
import me.tojatta.milk.ui.click.GuiClick;
import me.tojatta.milk.ui.click.elements.ElementBase;
import me.tojatta.milk.ui.click.theme.manager.ThemeManager;

import org.lwjgl.input.Mouse;

public class ElementComboBox extends ElementBase {
    
    public ElementComboBox(String... elements)
    {
        super(11);
        setElements(elements);
        setSelected(7);
    }

    public void drawElement(int par1, int par2, int par3, int par4) {    	
    	setPosX(par1);
    	setPosY(par2);	
    	
    	GuiUtils.drawGradientBorderedRect(getPosX(), getPosY(), getPosX() + getWidth(), getPosY() + getHeight(), 0.3F, 0xFF000000, 0xBB232323, 0xBB121212);
        
    	//Divinity.getInstance().getProximityFont().drawCenteredString(getElement(getSelected()), (getWidth()) / 2 + getPosX(), getPosY() + 4, 0xFFFFFFFF);
    	
    	if(getOpen()) {
    		for(int i = 0; i < getElements().length; i++) {
    			Milk.getInstance().getGuiFont().drawCenteredString(getElement(i), (getWidth()) / 2 + getPosX(), getPosY() + 4 + (i * 11), (getSelected() == i ? 0xFFFFFFFF : 0xFFDDDDDD));
    	
    			if(par3 >= getPosX() && par4 >= getPosY() + (i * 11) && par3 <= getPosX() + getWidth() && par4 <= getPosY() + 10 + (i * 11)) {
    				GuiUtils.drawRect(getPosX(), getPosY() + (i * 11), getPosX() + getWidth(), getPosY() + 11 + (i * 11), 0x22FFFFFF);
    			
    				if(Mouse.isButtonDown(0) && !getCheck()) {
    					setSelected(i);
    					if(!ThemeManager.getInstance().findTheme(getElement(i)).getState()) {
    						ThemeManager.getInstance().findTheme(getElement(i)).setState(true);
        	    			getWrapper().getPlayer().playSound("random.click", 1.0F, 1.0F);
    						Milk.getInstance().setClickGui(new GuiClick());
    						Milk.getInstance().getClickGui().loadPanels();
    					}else{
        	    			getWrapper().getPlayer().playSound("random.click", 1.0F, 1.0F);
    					}
    					setOpen(false);
    					setCheck2(true);
    				}
    			}
    		}
    		
    		if(!Mouse.isButtonDown(0) && getCheck()) {
    			setCheck(false);
			}
    	}else{
    		if(!Mouse.isButtonDown(0) && getCheck2()) {
    			setCheck2(false);
			}
    	}
    	
    	if(getOpen()) {
    		this.setHeight(11 * getElements().length + 1);
    	}else{
    		this.setHeight(12);
    	}
    }

	@Override
	public void mouseClicked(int par1, int par2, int par3) {
		if(par1 >= getPosX() && par2 >= getPosY() && par1 <= getPosX() + getWidth() && par2 <= getPosY() + getHeight() && par3 == 0) {
    		if(!getOpen() && !getCheck2()) {
    			setCheck(true);
    			setOpen(true);
        		
    			getWrapper().getPlayer().playSound("random.click", 1.0F, 1.0F);
    		}
    	}
	}
	
	private boolean COMBOBOX_OPEN;
	
	public boolean getOpen() {
		return COMBOBOX_OPEN;
	}
	
	public void setOpen(boolean OPEN) {
		this.COMBOBOX_OPEN = OPEN;
	}
	
	private boolean COMBOBOX_CHECK;
	
	public boolean getCheck() {
		return COMBOBOX_CHECK;
	}
	
	public void setCheck(boolean CHECK) {
		this.COMBOBOX_CHECK = CHECK;
	}
	
	private boolean COMBOBOX_CHECK2;
	
	public boolean getCheck2() {
		return COMBOBOX_CHECK2;
	}
	
	public void setCheck2(boolean CHECK2) {
		this.COMBOBOX_CHECK2 = CHECK2;
	}
	
	private int COMBOBOX_SELECTED;
	
	public int getSelected() {
		return COMBOBOX_SELECTED;
	}
	
	public String getElement(int ELEMENT) {
		return this.COMBOBOX_ELEMENTS[ELEMENT];
	}
	
	public void setSelected(int SELECTED) {
		this.COMBOBOX_SELECTED = SELECTED;
	}
	
	public String[] COMBOBOX_ELEMENTS;
	
	public String[] getElements() {
		return COMBOBOX_ELEMENTS;
	}
	
	public void setElements(String[] ELEMENTS) {
		this.COMBOBOX_ELEMENTS = ELEMENTS;
	}
}
