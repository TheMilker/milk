package me.tojatta.milk.ui.click.theme.themes.milk.panel;

import me.tojatta.milk.Milk;
import me.tojatta.milk.ui.GuiUtils;
import me.tojatta.milk.ui.click.panel.PanelBase;


public abstract class PanelBaseMilk extends PanelBase {

	protected PanelBaseMilk(String NAME, int POSX, int POSY, int WIDTH) {
		super(NAME, POSX, POSY, WIDTH);
	}
	
	protected PanelBaseMilk(String NAME, int POSX, int POSY) {
		super(NAME, POSX, POSY);
	}

	@Override
	public abstract void addElements();
	
	@Override
	public void drawScreen(int POSX, int POSY) {
		this.updatePanel(POSX, POSY);
		//Background
		GuiUtils.drawBorderedRect(getPosX(), getPosY(), getPosX() + getWidth(), getPosY() + getHeight(), 0.3F, 0xFF000000, 0xffE82517);
		//Pin buttons
		GuiUtils.drawBorderedRect(getPosX() + getWidth() - 14.0F, getPosY(), getPosX() + getWidth() - 3.0F, getPosY() + 15 - 3.0F, 0.3F, 0xFF000000, 0xff19A3B5);
		GuiUtils.drawBorderedRect(getPosX() + getWidth() - 27.0F, getPosY(), getPosX() + getWidth() - 16.0F, getPosY() + 15 - 3.0F, 0.3F, 0xFF000000, 0xff19A3B5);
		//Frame name
		Milk.getInstance().getGuiFont().drawString(getName(), getPosX() + 3, getPosY() + (getOpen() ? 4 : 5), 0xFFffffff);
		
		this.addElements();
		this.drawElements(POSX, POSY);
	}
}
