package me.tojatta.milk.ui.click.theme;

import me.tojatta.milk.ui.click.elements.ElementBase;
import me.tojatta.milk.ui.click.panel.PanelBase;
import me.tojatta.milk.ui.click.theme.manager.ThemeManager;


public class Theme {

	public Theme(boolean STATE, String NAME) {
		this.THEME_STATE = STATE;
		this.THEME_NAME = NAME;
	}
	
	private String THEME_NAME;
	
	public String getName() {
		return THEME_NAME;
	}
	
	public void setName(String NAME) {
		this.THEME_NAME = NAME;
	}
	
	private boolean THEME_STATE;
	
	public boolean getState() {
		return THEME_STATE;
	}
	
	public void setState(boolean STATE) {
		this.THEME_STATE = STATE;
		
		if(getState()) {		
			for(Theme THEME: ThemeManager.getInstance().getList()) {
				if(THEME != this) {
					THEME.setState(false);
				}
			}
			}else if(!getState()){			
				if(ThemeManager.getInstance().getCurrentTheme() == null) {
				ThemeManager.getInstance().findTheme("Milk").setState(true);
			}
		}
	}
	
	private PanelBase THEME_PANEL;
	
	public PanelBase getPanel() {
		return THEME_PANEL;
	}
	
	public void setPanel(PanelBase PANEL) {
		this.THEME_PANEL = PANEL;
	}

	private ElementBase THEME_BUTTON;
	
	public ElementBase getButton() {
		return THEME_BUTTON;
	}
	
	public void setButton(ElementBase BUTTON) {
		this.THEME_BUTTON = BUTTON;
	}

	private ElementBase THEME_COMBOBOX;
	
	public ElementBase getComboBox() {
		return THEME_COMBOBOX;
	}
	
	public void setComboBox(ElementBase BUTTON) {
		this.THEME_COMBOBOX = BUTTON;
	}
}
