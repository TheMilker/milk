package me.tojatta.milk.ui;

import me.tojatta.milk.Milk;
import me.tojatta.milk.Wrapper;
import me.tojatta.milk.events.EventGui;
import me.tojatta.milk.events.EventTick;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiIngame;

import com.darkmagician6.eventapi.EventManager;
import com.darkmagician6.eventapi.EventTarget;

public class MilkIngame extends GuiIngame{

	public MilkIngame(Minecraft minecraft) {
		super(minecraft);
		
		/**
		 * Registers the gui render event
		 */
		EventManager.register(this);
	}
	
	@Override
	public void renderGameOverlay(float par1, boolean par2, int par3, int par4){
		super.renderGameOverlay(par1, par2, par3, par4);
		
		/**
		 * Tick Event call
		 */
		EventTick eventTick = new EventTick();
		EventManager.call(eventTick);
		
		/**
		 * Renders only out of "f3" mode
		 */
		if(!Wrapper.getGS().showDebugInfo){
			
			/**
			 * Gui Render Event call
			 */
			EventGui eventGui = new EventGui();
			EventManager.call(eventGui);
		}
    }
	
	@EventTarget
	public void drawGui(EventGui event){
		Wrapper.getFR().drawStringWithShadow(Milk.getInstance().getCLIENTNAME()  + " v" + Milk.getInstance().getVERSION(), 2, 2, 0xffffffff);
	}

}
