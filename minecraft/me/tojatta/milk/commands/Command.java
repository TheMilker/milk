package me.tojatta.milk.commands;

import me.tojatta.milk.Wrapper;

public class Command extends Wrapper{
	
	private String command;
	private String desc;
	private String args;
	
	public Command(String command, String args){
		this.command = command;
		this.args = args;
		this.desc = "";
	}
	
	public Command(String command, String args, String desc){
		this.command = command;
		this.args = args;
		this.desc = desc;
	}
	
	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getArgs() {
		return args;
	}

	public void setArgs(String args) {
		this.args = args;
	}
	
	public void onCommand(String command, String[] args) {}
	
}
