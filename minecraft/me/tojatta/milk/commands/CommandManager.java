package me.tojatta.milk.commands;

import java.util.ArrayList;
import java.util.List;

import me.tojatta.milk.Wrapper;

public class CommandManager extends Wrapper{
	
	private static List<Command> commands = new ArrayList();
	private String prefix = ".";
	
	public static CommandManager getInstance(){
	    return new CommandManager();
	}
	
	public List<Command> getCommands(){
		return commands;
	}
	  
	public void addCommand(Command command){
		commands.add(command);
	}
	  
	public void removeCommand(Command command){
	    commands.remove(command);
	}
	  
	public String getPrefix(){
	    return this.prefix;
	}
	  
	public void setPrefix(String prefix){
	    this.prefix = prefix;
	}
}
