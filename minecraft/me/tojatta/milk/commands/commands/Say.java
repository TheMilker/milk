package me.tojatta.milk.commands.commands;

import net.minecraft.network.play.client.C01PacketChatMessage;
import me.tojatta.milk.commands.Command;

public class Say extends Command{

	public Say() {
		super("say", "say <msg>", "Says an unfiltered message in chat.");
	}
	public void onCommand(String command, String[] args){
	    sendPacket(new C01PacketChatMessage(command.substring(5)));
	}

}
