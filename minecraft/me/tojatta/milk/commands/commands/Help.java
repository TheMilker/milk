package me.tojatta.milk.commands.commands;

import me.tojatta.milk.commands.Command;
import me.tojatta.milk.commands.CommandManager;

public class Help extends Command{

	public Help() {
		super("help", "", "Shows information about commands.");
	}
	
	public void onCommand(String s, String[] a){
	    if (CommandManager.getInstance().getCommands().size() > 0) {
	      addChat("Listing Help (�c" + (CommandManager.getInstance().getCommands().size() - 1) + "�f):");
	      for (Command c : CommandManager.getInstance().getCommands()) {
	        if (c != this) {
	        	addChat(CommandManager.getInstance().getPrefix() + c.getArgs().replaceAll("<", "<�c").replaceAll(">", "�f>") + (c.getDesc() != "" ? " - " + c.getDesc() + (c.getDesc().endsWith(".") ? "" : ".") : "."));
	        }
	      }
	    }else{
	    	addChat("No commands loaded.");
	    }
	  }
}
