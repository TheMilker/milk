package me.tojatta.milk.events;

import com.darkmagician6.eventapi.events.Event;

public class EventRotation implements Event{
	
	private float yaw;
	private float pitch;
	  
	public EventRotation(float yaw, float pitch){
		this.yaw = yaw;
		this.pitch = pitch;
	}
	   
	public float getYaw(){
		return this.yaw;
	}
	 
	public float getPitch(){
		return this.pitch;
	}
  
	public void setRotation(Float yaw, Float pitch){
		if (yaw != null) {
			this.yaw = yaw.floatValue();
		}
		if (pitch != null) {
			this.pitch = pitch.floatValue();
		}
	}


}
